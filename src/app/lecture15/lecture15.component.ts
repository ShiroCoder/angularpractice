import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lecture15',
  templateUrl: './lecture15.component.html',
  styleUrls: ['./lecture15.component.css']
})
export class Lecture15Component implements OnInit {
    data = [
    {
      id: 1,
      name: 'author1'
    },
    {
      id: 2,
      name: 'author2'
    },
    {
      id: 3,
      name: 'author3'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
