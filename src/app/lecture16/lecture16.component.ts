import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lecture16',
  templateUrl: './lecture16.component.html',
  styleUrls: ['./lecture16.component.css']
})
export class Lecture16Component implements OnInit {

  textFormat = '';
  formatTextEvent(event) {
    this.textFormat = this.capitalizedString(event.target.value);
  }
  public capitalizedString(aString: string) {
    const sugar = require('sugar');
    const splitString = sugar.String.capitalize(aString, true, true).split(' ');
    for (let i = 1; i <= splitString.length - 1 ; i++ ) {
      if (splitString[i] === 'Of' || splitString[i] === 'The') {
        splitString[i] = splitString[i].toLowerCase();
      }
    }
    return splitString.join(' ');
  }

  constructor() { }

  ngOnInit() {
  }

}
