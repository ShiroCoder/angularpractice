import { TestBed } from '@angular/core/testing';

import { Lecture16Service } from './lecture16.service';

describe('Lecture16Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Lecture16Service = TestBed.get(Lecture16Service);
    expect(service).toBeTruthy();
  });
});
