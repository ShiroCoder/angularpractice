import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lecture17Component } from './lecture17.component';

describe('Lecture17Component', () => {
  let component: Lecture17Component;
  let fixture: ComponentFixture<Lecture17Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lecture17Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lecture17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
