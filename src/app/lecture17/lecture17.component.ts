import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-lecture17',
  templateUrl: './lecture17.component.html',
  styleUrls: ['./lecture17.component.css']
})
export class Lecture17Component implements OnInit {
 courses = [
	{
		lessons: ['lesson 1', 'lesson 2', 'lesson 3'],
		visibility: false
},
{
	lessons: ['lesson A', 'lesson B'],
	visibility: false
}
];



  // public setVisible(event) {
  //   event.target.value.visibility = false;
  // }
  // public setHidden(event) {
  //   event.target.value.visibility = true;
  //   console.log(event.target.visibility);
  // }


  constructor() { }

  ngOnInit() {
  }

}
