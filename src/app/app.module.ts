import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Lecture15Component } from './lecture15/lecture15.component';
import { Lecture16Component } from './lecture16/lecture16.component';
import { Lecture17Component } from './lecture17/lecture17.component';
import { Lecture18Component } from './lecture18/lecture18.component';

@NgModule({
  declarations: [
    AppComponent,
    Lecture15Component,
    Lecture16Component,
    Lecture17Component,
    Lecture18Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
