import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-lecture18',
  templateUrl: './lecture18.component.html',
  styleUrls: ['./lecture18.component.css']
})
export class Lecture18Component implements OnInit {
  loginForm  = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  red = false;
  green = false;
  onSubmit() {
    const req = this.loginForm.value;
    console.log(req);
    if (req.email === '123' && req.password === 'abc') {
      this.green = true;
      this.red = false;
    } else {
      this.red = true;
      this.green = false;
    }
  }


  constructor() { }

  ngOnInit() {
  }

}
